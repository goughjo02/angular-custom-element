import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Injector } from "@angular/core";
import { createCustomElement } from "@angular/elements";

import { AppComponent } from "./app.component";
import { ContactFormComponent } from "./contact-form/contact-form.component";

@NgModule({
  declarations: [AppComponent, ContactFormComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent, ContactFormComponent],
})
export class AppModule {
  constructor(private injector: Injector) {
    const el = createCustomElement(ContactFormComponent, { injector });
    customElements.define("contact-form", el);
  }
}
